import { LoaderService } from '../app/shared/loader.service';
import { FeatureModule } from './feature/feature.module';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ToastrModule ,ToastNoAnimationModule } from 'ngx-toastr';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from './shared/shared.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { InterceptorsService } from './core/Auth/Services/interceptors.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SharedModule,
    ToastrModule.forRoot(),
    // ToastNoAnimationModule.forRoot(),
  ],
  providers: [
     LoaderService,
    {provide : HTTP_INTERCEPTORS, useClass : InterceptorsService, multi : true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
