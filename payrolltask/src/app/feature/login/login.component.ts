import { AuthService } from '../../core/Auth/Services/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { map } from 'rxjs';
import { ToastrService  } from 'ngx-toastr';
import { LoaderService } from 'src/app/shared/loader.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form!: FormGroup
  isLoading: boolean = false;
  constructor(private fb:FormBuilder ,private route:Router, private auth:AuthService , private toastr:ToastrService , private loader:LoaderService){}
  mobailepattern = /[0-9\+\-\ ]/;
 ngOnInit(): void {
  this.form = this.fb.group({
    mobileno: ['',[Validators.required,Validators.pattern(this.mobailepattern)]],
    password: ['',[Validators.required,Validators.minLength(8)]]

  });
}
  Save(){
    const controls = this.form.controls;
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      return;
    }
    const authData = {
      username: controls['mobileno'].value,
      password: controls['password'].value
    };
    this.isLoading = true;
    this.auth.login(authData.username, authData.password)
      .pipe(map((user: any) => {
        // this.isLoading = false;
        this.loader.isloading
        if (user.success) {
          let accessToken = 'Basic' + btoa(authData.username + ':' + authData.password);
          localStorage.setItem('token', accessToken);
          localStorage.setItem('user', JSON.stringify(user));
          this.toastr.success("login successfully")
          this.route.navigate(['mytask']);
        }
        else
          this.toastr.error(user.errormessage)
      })
      ).subscribe();
  }
 }


