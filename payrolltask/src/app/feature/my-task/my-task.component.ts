import { TaskdailogComponent } from 'src/app/feature/taskdailog/taskdailog.component';
import { ToastrService } from 'ngx-toastr';
import { MytaskserrviceService } from 'src/app/core/Auth/Services/mytaskserrvice.service';
import { Component, OnInit, NgModule, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
@Component({
  selector: 'app-my-task',
  templateUrl: './my-task.component.html',
  styleUrls: ['./my-task.component.css'],
})
export class MyTaskComponent implements OnInit {
  constructor(
    private taskservice: MytaskserrviceService,
    private toaster: ToastrService,
    private dialog: MatDialog
  ) {}
  data: any;

  @ViewChild(MatPaginator) paginator: MatPaginator | undefined;
  @ViewChild(MatSort) sort: MatSort | undefined;
  displayedColumns = [
    'Title',
    'CustomerName',
    'AssignedBy',
    'AssignedDate',
    'DueDate',
    'Priority',
    'Status',
    'Action',
  ];

  Data = {
    From: 1,
    To: -1,
    Title: '',
    UserId: '',
    IsArchive: '',
    UserIds: [],
    Priority: '',
    TaskStatus: '',
    FromDueDate: '',
    ToDueDate: '',
    SortByDueDate: '',
  };

  ngOnInit(): void {
    this.GetMyTaskData();
  }

  GetMyTaskData() {
    this.taskservice.findMyTask(this.Data).subscribe((taskdata: any) => {
      this.data = new MatTableDataSource<any>(taskdata.data.TaskList);
      this.data.paginator = this.paginator;
      this.data.sort = this.sort;
    });
  }


  addTask() {
    const dialogRef = this.dialog.open(TaskdailogComponent, {
      width: '800px',
      height: '550px',
    });

    dialogRef.afterClosed().subscribe((result) => {
       this.GetMyTaskData()
    });
  }

  deleteTask(taskId: any) {
    let text = 'Do you want to delete this Task?';
    if (confirm(text) == true) {
      this.taskservice.deleteTask(taskId).subscribe((res) => {
        if (res) {
          this.toaster.success('Task Deleted Succesfully');
          this.GetMyTaskData();
        }
      });
    } else {
      return;
    }
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.data.filter = filterValue.trim().toLowerCase();
    if (this.data.paginator) {
      this.data.paginator.firstPage();
    }
  }
}
