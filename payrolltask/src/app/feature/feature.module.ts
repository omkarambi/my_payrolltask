import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { ToastrModule } from 'ngx-toastr';

import { FeatureRoutingModule } from './feature-routing.module';
import { MyTaskComponent } from './my-task/my-task.component';
import { LoginComponent } from './login/login.component';
import { TaskdailogComponent } from './taskdailog/taskdailog.component';
import { AddccdialogComponent } from './addccdialog/addccdialog.component';
import { MatNativeDateModule } from '@angular/material/core';
import { MatInputModule } from '@angular/material/input';
import { DatePipe } from '@angular/common';


@NgModule({
  declarations: [
    MyTaskComponent,
    LoginComponent,
    TaskdailogComponent,
    AddccdialogComponent
  ],
  imports: [
    FeatureRoutingModule,
    SharedModule,
    MatNativeDateModule,
    MatInputModule
  ],
  providers:[
    DatePipe,
  ]
})
export class FeatureModule { }
