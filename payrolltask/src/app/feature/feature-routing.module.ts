import { ReverseGuard } from './../core/Auth/reverse/reverse.guard';
// import { AuthGuard } from './../core/Auth/auth-guard/authentication.guard';
import { LoginComponent } from './login/login.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MyTaskComponent } from './my-task/my-task.component';
import { AuthenticationGuard } from '../core/Auth/auth-guard/authentication.guard';

const routes: Routes = [
  {path:"",canActivate:[ReverseGuard],component:LoginComponent},
  {path:"mytask",  canActivate:[AuthenticationGuard], component:MyTaskComponent},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FeatureRoutingModule { }
