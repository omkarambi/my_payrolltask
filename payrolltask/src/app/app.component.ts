import { Component } from '@angular/core';
import { AuthService } from './core/Auth/Services/auth.service';
import { LoaderService } from './shared/loader.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(public authservice : AuthService,
    public loaderservice : LoaderService) {
  }
  title = 'payrolltask';
}
