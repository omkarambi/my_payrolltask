import { ToastrService } from 'ngx-toastr';
import { MytaskserrviceService } from 'src/app/core/Auth/Services/mytaskserrvice.service';
import { Router } from '@angular/router';
import { AuthService } from './../../core/Auth/Services/auth.service';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { TaskdailogComponent } from 'src/app/feature/taskdailog/taskdailog.component';
import { map } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  constructor(
    private dialog: MatDialog,
    private auth: AuthService,
    private router: Router,
    private taskservice: MytaskserrviceService,
    public toaster :ToastrService
  ) {}
  param = {
    From: 1,
    To: -1,
    Text: '',
  };
  isLoading : boolean = false;
  ngOnInit(): void {}

  logout() {
    this.auth.logout();
    this.isLoading = true;

    this.router.navigate(['']);
    this.toaster.success("Logout Successful")
    this.isLoading = false;

  }
}


