// import { AuthGuard } from '../core/Auth/auth-guard/authentication.guard';

import { MytaskserrviceService } from './Auth/Services/mytaskserrvice.service';
import { AuthService } from './Auth/Services/auth.service';
import { NgModule } from '@angular/core';
import { NumberDirective } from './Auth/Directive/number.directive'
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { InterceptorsService } from './Auth/Services/interceptors.service';
import { OnlyalphabetDirective } from './Auth/Directive/onlyalphabet.directive';
// import { DatePipe } from './Auth/date.pipe';


@NgModule({
  declarations: [
    NumberDirective,
    OnlyalphabetDirective,
    // DatePipe,
  ],
  providers:[
    AuthService,
    MytaskserrviceService,
    {provide : HTTP_INTERCEPTORS, useClass : InterceptorsService, multi : true}
  ],
  exports:[
    NumberDirective,
    OnlyalphabetDirective
  ]
})
export class CoreModule { }
