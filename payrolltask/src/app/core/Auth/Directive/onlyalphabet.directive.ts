import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: 'input[appOnlyalphabet]'
})
export class OnlyalphabetDirective {

  constructor(private _el:ElementRef){}
  @HostListener('input', ['$event']) onInputChange(event:any) {
    const initalValue = this._el.nativeElement.value;
        this._el.nativeElement.value = initalValue.replace(/[^a-z^A-Z ]*/g, '');
//         if (initalValue !== this._el.nativeElement.value) {
//             event.stopPropagation();
//         }
  }

}
